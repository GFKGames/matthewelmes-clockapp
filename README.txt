ClockApp-MatthewElmes
Version 0.1
21/01/2021

ADD CLOCK:
Add clocks by clicking the "+" button on the canvas.

REMOVE CLOCK:
Remove clocks by clicking the "x" button on the clock.

CHANGE CLOCK MODE:
Change between clock modes by selecting a mode on the bottom panel of the clock.

SET 24 HOUR TIME:
Set 24 hour time by clicking the "24 hour time" checkbox in the Time Display panel.

SELECT TIME FORMAT:
Set the time format by selecting from the dropdown box on the Time Display panel.

SET CUSTOM TIME/DATE:
Set custom time by clicking the "set time" button in the Time Display panel.
Input a chosen date/time.
Use the dropdown box to select AM/PM/24 hour.
Click the "set time" button.

START STOPWATCH:
Start the stopwatch by clicking the "start" button on the Stopwatch panel.

STOP STOPWATCH:
Stop the stopwatch by clicking the "stop" button on the Stopwatch panel.

RESET STOPWATCH:
Reset the stopwatch by clicking the "reset" button on the Stopwatch panel.

SET TIMER:
Set the timer by clicking the "new timer" button.
Input desired timer length.
Click "set timer".

START TIMER:
Start the timer by clicking "start" in the Timer panel.

STOP TIMER:
Stop the timer by clicking "stop" in the Timer panel.
