﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ClockFunction : MonoBehaviour
{
    [Range(1,60)]
    [Tooltip("How many times per second the time is calculated. Less = better performance.")]
    [SerializeField]
    protected int ticksPerSecond;

    [SerializeField]
    protected Text clockFunctionText;

    virtual protected void PrintTime() { }

    virtual protected IEnumerator ClockFunctionTick()
    {
        //Sets how often to run the coroutine and creates a custom YieldInstruction.
        float delay = 1 / ticksPerSecond;
        WaitForSeconds timetoWait = new WaitForSeconds(delay);

        while (true)
        {
            PrintTime();

            yield return timetoWait;
        }
    }
}
