﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SetTimerButtonScript : MonoBehaviour
{
    private int totalTime = 0;

    [SerializeField]
    private InputField[] inputFields = new InputField[3];


    //Function is called when the 'set timer' button is pressed. Validates the number, and returns an int that 
    //is the calculation of the input fields' data in seconds.
    public int GetTimerTime()
    {
        int timeMultiplier = 3600; //3600 seconds in an hr
        totalTime = 0;

        bool valid = false;

        for(int i = 0; i < inputFields.Length; i++)
        {
            valid = Int32.TryParse(inputFields[i].text, out int number);

            if (!valid)
            {
                ErrorInputController.SetErrorMessage("Invalid format: Missing numbers in input fields.");
                break;
            }
            else
            {

                totalTime += number * timeMultiplier;
                timeMultiplier /= 60; //3600/60 = 60 (secs in a min)... 60/60 = 1 (sec in a sec).
            }
        }

        if (valid)
        {

            return totalTime;
        }

        return 0;
       
    }
}
