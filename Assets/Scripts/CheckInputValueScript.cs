﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CheckInputValueScript : MonoBehaviour
{
    [SerializeField]
    private int min = 0;

    public int Min
    {
        get
        {
            return min;
        }
        set
        {
            if (value >= 0 && value <= 1)
            {
                min = value;
            }
        }
    }

    //The maximum value for our use-case.
    [SerializeField]
    private int max = 0;

    public int Max
    {
        get
        {
            return max;
        }
        set
        {
            if(value >= 0 && value <= 23)
            {
                max = value;
            }
        }
    }

    private InputField field = null;

    public enum TimeType { Day, Month, Year, Hour, Minute, Zeroes, Default };

    public TimeType timeType = TimeType.Default;

    private void Awake()
    {
        field = GetComponent<InputField>();
    }

    private void OnEnable()
    {
        SetCurrentTime();
    }

    //This function validates the input into the input field. It does a min/max check when the input field reaches
    //the character limit for that field. It checks for the "-" symbol on the first input and removes it.
    public void CheckInput(InputField field)
    {
        if (field.text.Length == field.characterLimit)
        {
            bool isParsable = Int32.TryParse(field.text, out int number);

            if (!isParsable)
            {
                field.text = "";
            }
            else if (isParsable)
            {
                if (number < min || number > max)
                {
                    field.text = "";
                }
            }
        }

        else if (field.text == "-")
        {
            field.text = "";
        }
    }

    //This function sets some or all of the fields to default values based on an enum set on the input
    //field. It also pads out the string to the appropriate amount of integers.
    private void SetCurrentTime()
    {
        switch (timeType)
        {
            case TimeType.Day:
                field.text = DateTime.Now.Day.ToString("D2");
                break;
            case TimeType.Month:
                field.text = DateTime.Now.Month.ToString("D2");
                break;
            case TimeType.Year:
                field.text = DateTime.Now.Year.ToString("D4");
                break;
            case TimeType.Hour:
                field.text = DateTime.Now.Hour.ToString("D2");
                break;
            case TimeType.Minute:
                field.text = DateTime.Now.Minute.ToString("D2");
                break;
            case TimeType.Zeroes:
                field.text = "00";
                break;
            default:
                break;
        }
    }
}