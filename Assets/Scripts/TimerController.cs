﻿using UnityEngine;
using System;
using System.Timers;

public class TimerController : ClockFunction
{
    private AudioSource audioSource;

    private Timer timer = new Timer();

    [Tooltip("Time in milliseconds between timer ticks. 1000 = 1 sec.")]
    [Range(1,10000)]
    [SerializeField]
    private int timeInterval;

    private int countDownTime = 0;

    private TimeSpan timeSpanLeft;

    private UIEnableDisable uielementcontroller;

    //Cache the set timer panel.
    [SerializeField]
    private GameObject setTimerPanel;

    public enum TimerState { Running, Stopped, Default };

    private TimerState currentState = TimerState.Default;

    private SetTimerButtonScript setTimerScript;

    private ClockBackgroundController backgroundControllerScript;

    private void Awake()
    {
        uielementcontroller = GetComponentInParent<UIEnableDisable>();

        if(setTimerPanel != null)
        {
            setTimerScript = setTimerPanel.GetComponentInChildren<SetTimerButtonScript>();
        }
        else
        {
            print("Set timer script is null");
        }       

        audioSource = GetComponentInParent<AudioSource>();

        backgroundControllerScript = GetComponentInChildren<ClockBackgroundController>();

    }

    // Start is called before the first frame update
    void Start()
    {
        timer.Interval = timeInterval;

        timer.Elapsed += OnTimedEvent;

        PrintTime();
    }

    //Function stops the timer.
    public void StopTimer()
    {
        if (timer != null)
        {
            timer.Enabled = false;
            StopCoroutine(nameof(ClockFunctionTick)); //Don't need to calculate if the timer isn't running.
            currentState = TimerState.Stopped;
        }
        else
        {
            print("No timer.");
        }
    }

    //Function starts the timer.
    public void StartTimer()
    {
        if(countDownTime > 0)
        {
            if (timer != null)
            {
                timer.Enabled = true;
                StartCoroutine(nameof(ClockFunctionTick));
                currentState = TimerState.Running;
            }
            else
            {
                print("No timer.");
            }
        }
        else
        {
            ErrorInputController.SetErrorMessage("Timer is set to 0.");
        }
        
    }

    //Called when the 'set timer' button is pressed in the set timer panel. Sets the countdown variable.
    public void SetTimer()
    {
        if (setTimerScript != null)
        {
            countDownTime = setTimerScript.GetTimerTime();
        }
        else
        {
            print("SetTimerScript is null");
        }

        uielementcontroller.DisableUI(setTimerPanel);

        PrintTime();
    }

    //Event called every tick of the timeInterval variable.
    private void OnTimedEvent(object source, ElapsedEventArgs e)
    {
        if(countDownTime > 0)
        {
            countDownTime--;
        }      
    }

    //Function takes the countDownTime in seconds and converts to a TimeSpan. Using this
    //TimeSpan it sets the timerText in the desired format.
    protected override void PrintTime()
    {
        timeSpanLeft = TimeSpan.FromSeconds(countDownTime);

        clockFunctionText.text = timeSpanLeft.ToString(@"hh\:mm\.ss");

        if (currentState == TimerState.Running)
        {
            if (countDownTime <= 0)
            {
                if (audioSource != null)
                {
                    audioSource.Play();
                }

                SetAlarmOnColour(Color.red);

                Invoke(nameof(SetAlarmOffColour), audioSource.clip.length);

                StopTimer();
            }
        }
    }

    //Sets the colour of the clocks background image to a colour it takes as a parameter.
    private void SetAlarmOnColour(Color colour)
    {
        if(backgroundControllerScript != null)
        {
            backgroundControllerScript.SetColour(colour);
        }
        else
        {
            print("Background Controller script is null");
        }
        
    }

    //Sets the colour of the clocks background colour to its initial colour.
    private void SetAlarmOffColour()
    {
        if (backgroundControllerScript != null)
        {
            backgroundControllerScript.SetOriginalColour();
        }
        else
        {
            print("Background Controller script is null");
        }       
    }
}
