﻿using UnityEngine;
using UnityEngine.UI;

public class ErrorOutputController: MonoBehaviour
{
    //The error reporting text object.
    [SerializeField]
    private Text errorText = null;

    [Tooltip("Delay until the text is cleared.")]
    [Range(1,10)]
    public int timeToClear = 5;

    private void OnEnable()
    {
        ErrorInputController.SendError += SetText;
    }
    private void OnDisable()
    {
        ErrorInputController.SendError -= SetText;
    }

    //Function takes a string parameter and sets the text of the error reporting object. Invokes the clearing of that text.
    private void SetText(string text)
    {
        errorText.text = text;
        Invoke(nameof(ClearText), timeToClear);
    }

    private void ClearText()
    {
        errorText.text = "";
    }
}
