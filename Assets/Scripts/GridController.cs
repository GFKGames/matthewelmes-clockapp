﻿using UnityEngine;


public class GridController : MonoBehaviour
{
    [SerializeField]
    private GameObject clockToSpawn;

    private static int clockCount = 0;

    public static int ClockCount { get { return clockCount; } }

    // Start is called before the first frame update
    void Start()
    {
        //Cache a friendly number to use. Won't have to use transform.childCount.
        clockCount = transform.childCount - 1;
    }

    //Subtracts 1 from the length.
    public static bool CanClockBeDestroyed()
    {
        if (clockCount > 1)
        {
            clockCount--;
            return true;
        }

        return false;
    }

    //Instantiates a new clock as a child of this gameobject.
    public void AddToGrid()
    {
        GameObject go = Instantiate(clockToSpawn);

        //Sets its transform to take its parents' rect transform into account.
        go.transform.SetParent(transform, false);

        //Sets it as the 2nd to last child.
        go.transform.SetSiblingIndex(clockCount);

        clockCount++;
    }
}
