﻿
public static class ErrorInputController
{
    public delegate void ErrorAction(string error);
    public static event ErrorAction SendError;

    //Static function that takes a error message string as a parameter. It passes that
    //string on to the ErrorOutputController to modify the text data.
    public static void SetErrorMessage(string errorMessage)
    {
        SendError?.Invoke(errorMessage);
    }
}
