﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockBackgroundController : MonoBehaviour
{

    private Image m_image = null;

    private Color m_colour;

    [Tooltip("Set between 0 and 255")]
    [SerializeField]
    private float m_alpha;  

    private void Awake()
    {
        m_image = GetComponent<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Convert to a ratio of the max alpha.
        m_alpha /= 255;

        //Clamp between 0 and 1.
        m_alpha = Mathf.Clamp(m_alpha, 0, 1);

        m_colour = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), m_alpha);

        SetColour(m_colour);
    }

    //Set the colour of the image.
    public void SetColour(Color colour)
    {
        if(m_image != null)
        {
            m_image.color = colour;
        }
        else
        {
            print("Image is null");
        }
        
    }

    //Reverts the colour back to the cached initial random colour.
    public void SetOriginalColour()
    {
        SetColour(m_colour);
    }

}
