﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Globalization;

public class Clock : ClockFunction
{
    private DateTime theTime;

    private DateTime storedTime;

    private TimeSpan timeSinceLastCall;

    [SerializeField]
    private Dropdown timeFormatDropdown = null;

    [SerializeField]
    private GameObject setTimePanel;

    private SetTimeTextFieldController inputController;

    [SerializeField]
    private Toggle twentyFour;

    //The date format day/month/year.
    private CultureInfo enGB = new CultureInfo("en-GB");

    private UIEnableDisable uielementcontroller;

    private Dictionary<int, TimeFormat> timeFormat = new Dictionary<int, TimeFormat>();

    //Struct used for the look-up table.
    public struct TimeFormat
    {
        //Constructor takes the formats needed for the DateTime class for the specific time formats.
        public TimeFormat(string twelveHr, string twentyFourHr)
        {
            TwelveHrTime = twelveHr;
            TwentyFourHrTime = twentyFourHr;
        }
        public string TwelveHrTime { get; }
        public string TwentyFourHrTime { get; }
    }

    private void Awake()
    {
        if(setTimePanel != null)
        {
            inputController = setTimePanel.GetComponent<SetTimeTextFieldController>();
        }
        else
        {
            print("Set time panel has not been assigned.");
        }
        

        uielementcontroller = GetComponentInParent<UIEnableDisable>();
    }

    void Start()
    {
        InitialiseLookUpTable();
        
        theTime = DateTime.Now;
        storedTime = DateTime.UtcNow;

        StartCoroutine(nameof(ClockFunctionTick));

    }

    //Function takes a string as a parameter. It adds the string as a new element at bottom of the dropdown time format menu.
    private void AddToDropdownOptions(string optionsText)
    {
        timeFormatDropdown.options.Add(new Dropdown.OptionData() { text = optionsText });
    }

    //Function initialises the values for the Time Format look-up table.
    private void InitialiseLookUpTable()
    {
        //Clear old menu options if left in the inspector.
        timeFormatDropdown.ClearOptions();

        timeFormat.Add(0, new TimeFormat("hh:mm.ss tt", "HH:mm.ss")); //Add an instance of TimeFormat class with the 'first' being the 12hr time and the 'second' being 24hr time.
        AddToDropdownOptions("12:00.00"); //Add the text to accompany this time format to the dropdown box.

        timeFormat.Add(1, new TimeFormat("dd'/'MM'/'yyyy hh:mm.ss tt", "dd'/'MM'/'yyyy HH:mm.ss"));
        AddToDropdownOptions("01/01/2021 12:00.00");

        timeFormat.Add(2, new TimeFormat("dddd, dd MMMM yyyy hh:mm.ss tt", "dddd, dd MMMM yyyy HH:mm.ss tt"));
        AddToDropdownOptions("Monday 1 January, 2021 12:00.00");

        timeFormat.Add(3, new TimeFormat("ddd dd MMM yyy hh:mm.ss tt", "ddd dd MMM yyy HH:mm.ss tt"));
        AddToDropdownOptions("Mon. 1 Jan. 2021 12:00.00");

        timeFormat.Add(4, new TimeFormat("MMM dd hh:mm tt", "MMM dd HH:mm tt"));
        AddToDropdownOptions("Jan. 1 12:00");

        //Small bug where it won't initialise with text unless there is a value change.
        timeFormatDropdown.value = 1;
        timeFormatDropdown.value = 0;
    }

    //This function is called when you press 'set time' in the set time panel. It grabs the values from the
    //input controller and passes them to the SetTime() function.
    public void GetSetTime()
    {
        if(inputController != null)
        {
            SetTime(inputController.GetTimeValues());
        }
        else
        {
            print("Input controller is null");
        }
       
    }

    //This function formats a string from an array of strings. It takes that formatted string and validates the resultant DateTime data-type.
    //This will take into account things such as leap years etc. It then sets the valid DateTime as the classes time to be printed to screen.
    public void SetTime(string[] timeValues)
    {

        DateTime temp;
        
        //Create a formatted string (year/month/day/hour/minute) required to construct a DateTime class.
        string formattedDateTime = string.Format("{2}/{1}/{0} {3}:{4}",
                         timeValues[2], timeValues[1], timeValues[0], timeValues[3], timeValues[4]);

        if (DateTime.TryParseExact(formattedDateTime, "dd/MM/yyyy HH:mm", enGB, DateTimeStyles.None, out temp))
        {
            theTime = temp;

            if(uielementcontroller != null)
            {
                uielementcontroller.DisableUI(setTimePanel);
            }          
        }
        else
        {
            ErrorInputController.SetErrorMessage("Invalid date or time.");
        }       
    }

    //Is called when the reset to local button is pressed on the clock. Sets the time to the current local time.
    public void SetLocalTime()
    {
        theTime = DateTime.Now;
    }

    //This function checks the delta time between the current UTC time and the last stored time. It then adds that
    //Timespan to the cached theTime to give the current local time. Depending on the selected format in the
    //time format dropdown it modifies text to show the appropriate data on screen by using the .
    protected override void PrintTime()
    {
        timeSinceLastCall = DateTime.UtcNow - storedTime;

        theTime += timeSinceLastCall;

        storedTime = DateTime.UtcNow;

        if (timeFormatDropdown != null && twentyFour != null)
        {
            //Uses the index value of the currently selected time format in the dropdown box.
            if (timeFormat.TryGetValue(timeFormatDropdown.value, out TimeFormat value))
            {
                if (!twentyFour.isOn)
                {
                    clockFunctionText.text = theTime.ToString(value.TwelveHrTime);
                }
                else
                {
                    clockFunctionText.text = theTime.ToString(value.TwentyFourHrTime);
                }
            }
        }
        else
        {
            print("Dropdown box is null");
        }
    }
}
