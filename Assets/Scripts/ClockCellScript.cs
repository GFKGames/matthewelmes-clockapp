﻿using UnityEngine;


public class ClockCellScript : MonoBehaviour
{
    //Function to destroy the clock.
    public void DestroyMe()
    {
        //Returns true if there is more than 1 clock on the canvas.
        if(GridController.CanClockBeDestroyed())
        {
            Destroy(transform.gameObject);
        }   
        else
        {
            ErrorInputController.SetErrorMessage("Cannot remove the last clock.");
        }
    }
}
