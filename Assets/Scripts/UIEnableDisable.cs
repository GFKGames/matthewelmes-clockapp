﻿using UnityEngine;

public class UIEnableDisable : MonoBehaviour
{
    public GameObject[] featurePanels = new GameObject[3];

    //Function that ensures only 1 panel is open at any one time.
    public void CloseAllOtherPanels(int indexToOpen)
    {
        for(int i = 0; i < featurePanels.Length; i++)
        {
            if(i != indexToOpen)
            {
                DisableUI(featurePanels[i]);
            }
            else
            {
                EnableUI(featurePanels[i]);
            }
        }
    }

    //Function enables a gameobject.
    public void EnableUI(GameObject UIElement)
    {
        if(UIElement != null)
        {
            UIElement.SetActive(true);
        }      
    }

    //Function disables a gameobject.
    public void DisableUI(GameObject UIElement)
    {
        if(UIElement != null)
        {
            UIElement.SetActive(false);
        }        
    }
}
