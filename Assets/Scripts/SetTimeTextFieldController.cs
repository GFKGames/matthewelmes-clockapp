﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SetTimeTextFieldController : MonoBehaviour
{
    [SerializeField]
    private InputField[] timeValuesArray = new InputField[5];

    [SerializeField]
    private Dropdown timePeriod;

    private string[] timeValues = new string[5];

    //Is called when the time period dropdown event is called in the 'set time' panel. Sets the minimum
    //and maximum values for the hour input field.
    public void SetMaxTimeValue()
    {
        //Cache the 'hour' input fields' CheckInputValueScript.
        CheckInputValueScript inputValueScript = timeValuesArray[3].GetComponent<CheckInputValueScript>();

        //If in 24-hr time format.
        if (timePeriod.value == 2)
        {           
            inputValueScript.Max = 23;
            inputValueScript.Min = 0;
        }
        //If in AM/PM format.
        else
        {
            inputValueScript.Max = 12;
            inputValueScript.Min = 1;
        }
    }

    //Called by the Clock class' GetSetTime() to get the values from the input fields. It returns the values
    //of the input fields in the correct format of day/month/year/hour in 24hr time/min. If the data is missing
    //numbers an empty array is returned.
    public string[] GetTimeValues()
    {
        for (int i = 0; i < timeValuesArray.Length; i++)
        {
            if (timeValuesArray[i].text.Length != timeValuesArray[i].characterLimit)
            {
                ErrorInputController.SetErrorMessage("Numbers are missing from the date/time");
                return new string[5];
            }
            else
            {
                timeValues[i] = timeValuesArray[i].text;
            }           
        }

        //In the AM time period if the hour value is 12 change it to 00.
        if (timePeriod.value == 0)
        {      
            if(timeValues[3] == "12")
            {
                timeValues[3] = "00";
            }
        }

        //In the PM time period the hour will have 12 added to it, otherwise will be set to 12.
        if(timePeriod.value == 1)
        {
            if(Int32.TryParse(timeValuesArray[3].text, out int hour))
            {
                timeValues[3] = hour < 12 ? (hour + 12).ToString() : "12";
            }
        }

        //Clear the hour and minute text so the placeholder text takes effect.
        timeValuesArray[3].text = ""; 
        timeValuesArray[4].text = ""; 

        return timeValues;
    }
}
