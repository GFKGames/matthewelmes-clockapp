﻿using UnityEngine;
using System;
using System.Diagnostics;
using UnityEngine.UI;

public class StopwatchController : ClockFunction
{
    private Stopwatch m_stopWatch = new Stopwatch();

    [SerializeField]
    private Text startStopButtonText = null;

    private TimeSpan elapsedTime;

    public enum StopWatchState { Running, Stopped, Default};

    private StopWatchState currentState = StopWatchState.Default;

    void Start()
    {
        ChangeTextColour(startStopButtonText, Color.green);    

    }

    //Function is called when the start/stop button is pressed on the stopwatch panel. It either starts or stops
    //the stopwatch based on its current state. It also changes the text to reflect the buttons' current function.
    public void StartStopStopWatch()
    {
        if (m_stopWatch != null) 
        {
            if (currentState != StopWatchState.Running)
            {
                m_stopWatch.Start();
                ChangeText(startStopButtonText, "Stop");
                ChangeTextColour(startStopButtonText, Color.red);
                StartCoroutine(nameof(ClockFunctionTick));
                currentState = StopWatchState.Running;
            }
            else if (currentState == StopWatchState.Running)
            {
                m_stopWatch.Stop();
                ChangeText(startStopButtonText, "Start");
                ChangeTextColour(startStopButtonText, Color.green);
                StopCoroutine(nameof(ClockFunctionTick)); //Don't need to print to screen when there is no updated data.
                currentState = StopWatchState.Stopped;

            }           
        }
        else
        {
            print("Missing stopwatch.");
        }
    }

    //Function to change the text of a text object.
    private void ChangeText(Text text, string newText)
    {
        if(text != null)
        {
            text.text = newText;
        }
        else
        {
            print("Null text.");
        }
    }

    //Function to change the Text colour of a text object.
    private void ChangeTextColour(Text text, Color colour)
    {
        if (text != null)
        {
            text.color = colour;
        }
        else
        {
            print("Null text.");
        }
    }

    //Function to reset the stopwatch. Also modifies the text of the start/stop button
    //to reflect current functionality.
    public void ResetStopWatch()
    {
        if (m_stopWatch != null)
        {
            m_stopWatch.Reset();
            ChangeText(startStopButtonText, "Start");
            ChangeTextColour(startStopButtonText, Color.green);
            PrintTime();
            StopCoroutine(nameof(ClockFunctionTick));
            currentState = StopWatchState.Default;
        }
        else
        {
            print("Missing stopwatch.");
        }
    }

    //Function that formats the elapsed time of the stopwatch, and passes the string
    //to the text object.
    protected override void PrintTime()
    {
        elapsedTime = m_stopWatch.Elapsed;

        string elapsedTimeString = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            elapsedTime.Hours, elapsedTime.Minutes, elapsedTime.Seconds, elapsedTime.Milliseconds / 10);

        clockFunctionText.text = elapsedTimeString;
    }
}
